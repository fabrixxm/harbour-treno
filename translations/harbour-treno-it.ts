<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT" sourcelanguage="it">
<context>
    <name>AvvisiPage</name>
    <message>
        <source>Refresh</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <source>News</source>
        <translation>Avvisi</translation>
    </message>
</context>
<context>
    <name>AvvisoDetailPage</name>
    <message>
        <source>Open web page</source>
        <translation>Apri pagina web</translation>
    </message>
</context>
<context>
    <name>BookmarkSectionHeading</name>
    <message>
        <source>Trains</source>
        <translation>Treni</translation>
    </message>
    <message>
        <source>Stations</source>
        <translation>Stazioni</translation>
    </message>
</context>
<context>
    <name>Bookmarks</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Search train #</source>
        <translation>Cerca treno per numero</translation>
    </message>
    <message>
        <source>No bookmarks yet.
Pull down to search.</source>
        <translation>Nessun preferito.
Trascina verso il basso per cercare.</translation>
    </message>
    <message>
        <source>Search station</source>
        <translation>Cerca stazione</translation>
    </message>
    <message>
        <source>Remove from bookmarks</source>
        <translation>Rimuovi dai preferiti</translation>
    </message>
    <message>
        <source>Removing bookmark</source>
        <translation>Rimuovendo preferito</translation>
    </message>
    <message>
        <source>News</source>
        <translation>Avvisi</translation>
    </message>
    <message>
        <source>Real time lines</source>
        <translation>Linee in tempo reale</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>delay</source>
        <translation>ritardo</translation>
    </message>
</context>
<context>
    <name>LineePage</name>
    <message>
        <source>Refresh</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <source>Real time lines</source>
        <translation>Linee in tempo reale</translation>
    </message>
</context>
<context>
    <name>SearchStation</name>
    <message>
        <source>Search station</source>
        <translation>Cerca stazione</translation>
    </message>
    <message>
        <source>Station name</source>
        <translation>Nome stazione</translation>
    </message>
</context>
<context>
    <name>SearchTrain</name>
    <message>
        <source>Search train by number</source>
        <translation>Cerca treno per numero</translation>
    </message>
    <message>
        <source>Train number</source>
        <translation>Numero treno</translation>
    </message>
</context>
<context>
    <name>StazionePage</name>
    <message>
        <source>Refresh</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <source>departures</source>
        <translation>partenze</translation>
    </message>
    <message>
        <source>arrivals</source>
        <translation>arrivi</translation>
    </message>
    <message>
        <source>Show departures</source>
        <translation>Mostra partenze</translation>
    </message>
    <message>
        <source>Show arrivals</source>
        <translation>Mostra arrivi</translation>
    </message>
    <message>
        <source>Add to bookmarks</source>
        <translation>Aggiungi ai preferiti</translation>
    </message>
    <message>
        <source>Remove bookmark</source>
        <translation>Rimuovi dai preferiti</translation>
    </message>
</context>
<context>
    <name>StopItem</name>
    <message>
        <source>actual: %1</source>
        <translation>effettivo: %1</translation>
    </message>
    <message>
        <source>expected: %1</source>
        <translation>previsto: %1</translation>
    </message>
    <message>
        <source>delay: %1</source>
        <translation>ritardo: %1</translation>
    </message>
</context>
<context>
    <name>TrainItem</name>
    <message>
        <source>%2 : to %1</source>
        <translation>%2 : per %1</translation>
    </message>
    <message>
        <source>%2 : from %1</source>
        <translation>%2 : da %1</translation>
    </message>
</context>
<context>
    <name>TrenoModel</name>
    <message>
        <source>%1 at %2</source>
        <translation>%1 alle %2</translation>
    </message>
    <message>
        <source>delay: %1 min</source>
        <translation>ritardo: %1 min</translation>
    </message>
</context>
<context>
    <name>TrenoPage</name>
    <message>
        <source>Refresh</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <source>Add to bookmarks</source>
        <translation>Aggiungi ai preferiti</translation>
    </message>
    <message>
        <source>Remove bookmark</source>
        <translation>Rimuovi dai preferiti</translation>
    </message>
</context>
</TS>
