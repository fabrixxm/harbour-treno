<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AvvisiPage</name>
    <message>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>News</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AvvisoDetailPage</name>
    <message>
        <source>Open web page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BookmarkSectionHeading</name>
    <message>
        <source>Trains</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stations</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Bookmarks</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Search train #</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No bookmarks yet.
Pull down to search.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search station</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove from bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removing bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>News</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Real time lines</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>delay</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LineePage</name>
    <message>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Real time lines</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchStation</name>
    <message>
        <source>Search station</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Station name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchTrain</name>
    <message>
        <source>Search train by number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Train number</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StazionePage</name>
    <message>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>departures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>arrivals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show departures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show arrivals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add to bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove bookmark</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StopItem</name>
    <message>
        <source>actual: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>expected: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>delay: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrainItem</name>
    <message>
        <source>%2 : to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%2 : from %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrenoModel</name>
    <message>
        <source>%1 at %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>delay: %1 min</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrenoPage</name>
    <message>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add to bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove bookmark</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
