# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-treno

CONFIG += sailfishapp

SOURCES += src/harbour-treno.cpp

DISTFILES += qml/harbour-treno.qml \
    qml/cover/CoverPage.qml \
    rpm/harbour-treno.spec \
    rpm/harbour-treno.yaml \
    translations/*.ts \
    harbour-treno.desktop \
    qml/pages/Bookmarks.qml \
    qml/pages/SearchTrain.qml \
    qml/models/TrenoModel.qml \
    qml/pages/TrenoPage.qml \
    qml/utils.js \
    qml/models/HTMLListModel.qml \
    qml/elements/BookmarkSectionHeading.qml \
    qml/models/JSONListModel.qml \
    qml/models/jsonpath.js \
    qml/pages/SearchStation.qml \
    qml/elements/StopGraph.qml \
    qml/elements/LoadingSpinner.qml \
    qml/pages/StazionePage.qml \
    qml/elements/StopItem.qml \
    qml/models/StazioneModel.qml \
    qml/elements/TrainItem.qml \
    qml/elements/BookmarkItem.qml \
    qml/harbour-treno-symbolic.svg \
    qml/models/AvvisiModel.qml \
    qml/pages/AvvisiPage.qml \
    qml/elements/AvvisoItem.qml \
    qml/pages/AvvisoDetailPage.qml \
    qml/pages/LineePage.qml \
    qml/elements/LineaItem.qml \
    qml/pages/LineaDetailPage.qml \
    qml/elements/LineaNewsItem.qml \
    rpm/harbour-treno.changes.run.in \
    rpm/harbour-treno.changes.in

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/harbour-treno-it.ts
