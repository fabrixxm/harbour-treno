import QtQuick 2.0
import Sailfish.Silica 1.0
import "../elements"

CoverBackground {

    Column {
        anchors {
            topMargin: Theme.paddingSmall
            leftMargin: Theme.paddingSmall
            rightMargin: Theme.paddingSmall
            fill: parent
        }


        visible: globals.train !== false

        spacing: Theme.paddingMedium

        Label {
            id: trainLabel
            width: parent.width
            text: globals.train ? globals.train.name : ""
            truncationMode: TruncationMode.Fade
            horizontalAlignment: Text.AlignHCenter
        }


        Item {
            width: parent.width
            height: Math.max(stopGraph.height, stationLabel.height + stationTime.height + Theme.paddingSmall)

            StopGraph {
                anchors.bottom: undefined
                id: stopGraph
                info: globals.train ? globals.train.current : {}
                width: 64
                height: 128
            }

            Label {
                id: stationLabel
                anchors {
                    top: parent.top
                    left: stopGraph.right
                    right: parent.right
                    leftMargin: Theme.paddingSmall
                }
                text:  globals.train ? globals.train.current.station : ""
                wrapMode: Text.WordWrap
            }

            Label {
                id: stationTime
                anchors {
                    top: stationLabel.bottom
                    right: parent.right
                    topMargin: Theme.paddingSmall
                }
                text: globals.train ? globals.train.current.real : ""
                font.pixelSize: Theme.fontSizeTiny
            }
        }

        Label {
            width: parent.width
            text: qsTr("delay")
            font.pixelSize: Theme.fontSizeTiny
            truncationMode: TruncationMode.Fade
            horizontalAlignment: Text.AlignHCenter
            visible: globals.train && globals.train.current.delay > 0
        }

        Label {
            width: parent.width
            text: globals.train ? globals.train.current.delay : ""
            truncationMode: TruncationMode.Fade
            horizontalAlignment: Text.AlignHCenter
            visible: globals.train && globals.train.current.delay > 0
        }
    }

    Image {
        source: "/usr/share/icons/hicolor/128x128/apps/harbour-treno.png"
        anchors.centerIn: parent
        visible: globals.train === false
    }


    CoverActionList {
        id: coverAction

        CoverAction {
            iconSource: "image://theme/icon-cover-search"
        }
    }
}
