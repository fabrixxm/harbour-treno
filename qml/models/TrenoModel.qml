import QtQuick 2.0
import QtQuick.XmlListModel 2.0

import "../utils.js" as Utils

Item {
    property ListModel model: ListModel { id: listModel }

    property alias count: listModel.count
    property int status: {
        if (htmlModel.getTheUrl() === "") return -1;
        return htmlModel.xhrstatus;
    }
    property variant current

    HTMLListModel {
        id: htmlModel
        query: '/html/body/div[@class]'

        function getTheUrl() {
            var theurl = "/scheda?dettaglio=visualizza&tipoRicerca=numero&lang=IT";
            theurl = theurl + "&numeroTreno=" + numeroTreno;
            theurl = theurl + "&codLocOrig=" + codLocOrig;
            if (numeroTreno == "" || codLocOrig == "") return "";
            return theurl;
        }

        function fixHtml(html) {
            return html
                    .trim()
                    .replace(/\r\n/g, '')
                    .replace(/  +|\t/g, ' ')
                    .replace(/<br> <br> Binario/g, '</p><br /> Binario')
                    .replace(/<br>/g, '<br />')
                    .replace(/&/g, '&amp;');
        }

        onReady: populate();

        XmlRole { name: "status"; query: "@class/string()" }
        XmlRole { name: "station"; query: "h2/string()" }
        XmlRole { name: "program"; query: "p[1]/strong/string()" }
        XmlRole { name: "real"; query: "p[2]/strong/string()" }
    }

    property string name: ""
    property string numeroTreno: ""
    property string codLocOrig: ""


    function reload() {
        htmlModel.update();
    }

    function populate() {
        listModel.clear();
        if (htmlModel.getTheUrl() === "") return;
        // console.log("xml: '" + htmlModel.xml + "'");
        var _name = htmlModel.xml.match(/<h1>([^<]+)<\/h1>/);
        name = _name[1];


        var count = htmlModel.count;
        console.log("html count", count);
        for(var k=0; k<count; k++) {
            var i = htmlModel.get(k);
            var o = {
                'station' : i.station.trim(),
                'status': i.status.trim(),
                'program': i.program.trim(),
                'real': i.real.trim(),
                't_program': Utils.hm2min(i.program.trim()),
                't_real': Utils.hm2min(i.real.trim()),
                'current': false,
                'delay': 0,
                'first': false,
                'last': false,

                'formatDescription' : function() {
                    var desc = qsTr("%1 at %2").arg(o.station).arg(o.real)
                    if (o.delay > 0) {
                        desc = desc + ", " + qsTr("delay: %1 min").arg(o.delay);
                    }
                    return desc;
                }
            }

            o.delay = o.t_real - o.t_program;

            if (k+1 === count && i.status === "giaeffettuate") o.current = true;
            if (k+1 < count && i.status === "giaeffettuate" && htmlModel.get(k+1).status !== "giaeffettuate" ) o.current = true;

            if (k==0 && o.current && o.real === "") {
                o.status = ""
                o.current = false
            }

            if (o.current) current = o;

            //console.log("Item", k, "ritardo")
            //for(var z in o) { console.log(z, ":", o[z]); }

            if (k === 0) o.first = true
            if (k+1 === count) o.last = true

            listModel.append(o);
        }
        htmlModel.xhrstatus = 5;
    }

}
