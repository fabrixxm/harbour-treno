import QtQuick 2.0
import QtQuick.XmlListModel 2.0

import "../utils.js" as Utils

XmlListModel {
    id: model
    xml: ""
    query: "/html/body/div[@class]"
    namespaceDeclarations: "declare default element namespace 'http://www.w3.org/1999/xhtml';"

    property string method: "GET"
    property int xhrstatus: 0

    signal ready();

    onStatusChanged: {
        console.log("HTMLListModel status", status, errorString());
        if (getTheUrl() !== "" && status === XmlListModel.Ready && xhrstatus === 4) ready();
    }

    function update() {
        console.log("HTMLListModel.update()");
        xhrstatus = 0;
        var theurl = getTheUrl();
        if (theurl === "") { console.log("empty url, return"); return; }
        theurl = globals.baseurl + theurl;

        var xhr = new XMLHttpRequest;
        xhr.onreadystatechange = function() {
            xhrstatus = xhr.readyState;
            // status = xhr.status
            if (xhr.readyState === XMLHttpRequest.DONE) {
                xml = fixHtml(xhr.responseText);
//                console.log("#######################################");
//                console.log(xml);
                reload();
            }
        }

        if (method === "GET") {
            xhr.open("GET", theurl, true);
            console.log("GET" , theurl);
            xhr.send();
        }
        if (method === "POST") {
            xhr.open("POST", theurl, true);
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            var params = _getPostData();
            console.log("POST" , theurl);
            console.log(params);
            xhr.send(params);
        }
    }

    function _getPostData() {
        var data = getThePostData();
        var params=[];
        for(var k in data) {
            if (data[k]!=="") {
                params.push(encodeURIComponent(k)+"="+encodeURIComponent(data[k]));
            }
        }
        return params.join("&");
    }

    function getThePostData() {
        return {};
    }

    function getTheUrl() {
        return "";
    }

    function fixHtml(html) {
        return html;
    }
}
