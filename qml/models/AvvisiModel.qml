import QtQuick 2.0
import QtQuick.XmlListModel 2.0

XmlListModel {
    id: xmlModel
    source: "http://www.trenord.it/it/rss-avvisi.aspx"
    query: "/rss/channel/item"
    namespaceDeclarations: "declare namespace content = 'http://purl.org/rss/1.0/modules/content/';"

    XmlRole { name: "title"; query: "title/string()" }
    XmlRole { name: "pubDate"; query: "pubDate/string()" }
    XmlRole { name: "link"; query: "link/string()" }
    XmlRole { name: "content"; query: "content:encoded/string()" }
    XmlRole { name: "guid"; query: "guid/string()"; isKey: true }
}
