import QtQuick 2.0
import QtQuick.XmlListModel 2.0

import "../utils.js" as Utils

Item {
    property ListModel model: ListModel { id: listModel }

    property alias count: listModel.count
    property int status: {
        if (htmlModel.getTheUrl() === "") return -1;
        return htmlModel.xhrstatus;
    }

    HTMLListModel {
        method: "POST"
        id: htmlModel
        query: '/html/body/div[@class="bloccorisultato"]'

        function getTheUrl() {
            var theurl = "/stazione";
            if (stazione == "" && codiceStazione == "") return "";
            return theurl;
        }


        function getThePostData() {
            return {
                "lang":"IT",
                "stazione":stazione,
                "codiceStazione":codiceStazione
            }
        }


        function fixHtml(html) {
            html = html
                    .trim()
                    .replace(/\r\n/g, '')
                    .replace(/  +|\t/g, ' ')
                    .replace(/<br>/g, '<br />')
                    .replace(/&/g, '&amp;');

            if (timetable == "departures") {
                html = html.replace(/<p><strong>Arrivi<\/strong><\/p>.*<\/body>/, "</div></body>" );
            } else {
                html = html.replace(/<\/h1>.*<p><strong>Arrivi<\/strong><\/p>/, "</h1><div>");
            }

            return html;
        }

        onReady: populate();

        XmlRole { name: "destination"; query: "div/strong[1]/string()" }
        XmlRole { name: "departure"; query: "div/strong[2]/string()" }
        XmlRole { name: "train"; query: "h2/string()" }
        XmlRole { name: "link"; query: "a/@href/string()" }
    }

    property string timetable: "departures"
    property string name: ""
    property string stazione: ""
    property string codiceStazione: ""


    function reload() {
        htmlModel.update();
    }

    function populate() {
        listModel.clear();
        if (htmlModel.getTheUrl() === "") return;
        name = htmlModel.xml.match(/<h1>([^<]+)<\/h1>/)[1].replace("Stazione di ", "");

        var count = htmlModel.count;
        console.log("html count", count);
        for(var k=0; k<count; k++) {
            var i = htmlModel.get(k);
            var o = {
                'name': i.train.trim(),
                'to' : i.destination.trim(),
                'start': i.departure.trim(),
                't_start': Utils.hm2min(i.departure.trim()),
                'link': i.link.trim()
            }
            // set link query parameters in o object
            i.link.trim().split("?")[1].split("&").forEach(function(s){ var t = s.split("="); o[t[0]] = t[1] });
            /*
            console.log(k)
            for(var z in o) {
                console.log(z, "=", o[z]);
            }
            */
            listModel.append(o);
        }
        htmlModel.xhrstatus = 5;
    }

}
