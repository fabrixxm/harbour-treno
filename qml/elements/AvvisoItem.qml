import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {
    id: delegate
    anchors {
        left: parent.left
        right: parent.right
    }
    contentHeight: column.height + (Theme.paddingSmall * 2)

    Column {
        id: column
        spacing: Theme.paddingSmall
        anchors {
            leftMargin: Theme.paddingLarge
            rightMargin: Theme.paddingLarge
            topMargin: Theme.paddingSmall
            left: parent.left
            right: parent.right
            top:parent.top
        }
        Label {
            text: model.title
            width: parent.width
            color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
            truncationMode: TruncationMode.Elide
        }
        Label {
            text: model.pubDate
            width: parent.width
            color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
            font.pixelSize: Theme.fontSizeTiny
            truncationMode: TruncationMode.Fade
        }
    }
}
