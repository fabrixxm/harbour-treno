import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {
    id: delegate
    anchors {
        left: parent.left
        right: parent.right
    }
    contentHeight: column.height + (Theme.paddingSmall * 2)

    Rectangle {
        anchors {
            left: parent.left
        }
        x:0
        y:Theme.paddingSmall
        width: 5
        height: delegate.contentHeight - (Theme.paddingSmall*2);

        color: [Theme.highlightColor, Theme.highlightColor, "#e6e600", "#e60000"][model.severity_code]

    }

    Column {
        id: column
        spacing: Theme.paddingSmall
        anchors {
            leftMargin: Theme.paddingLarge
            rightMargin: Theme.paddingLarge
            topMargin: Theme.paddingSmall
            left: parent.left
            right: parent.right
            top:parent.top
        }
        Label {
            text: model.date
            width: parent.width
            font.pixelSize: Theme.fontSizeTiny
            truncationMode: TruncationMode.Fade
        }
        Label {
            text: model.description
            width: parent.width
            wrapMode: Text.Wrap
        }

    }
}
