import QtQuick 2.0

import "../utils.js" as Utils

Item {
    id: stopGraph

    property variant info: ({})
    property int size: 32
    property int ratio: 4

    property int now: 0

    Timer {
        interval: 10000 // 10 seconds
        repeat: true
        running: true
        triggeredOnStart: true
        onTriggered: {
            now = Utils.currentmin()
        }
    }

    anchors {
        top: parent.top
        left: parent.left
        bottom: parent.bottom
    }
    width: height

    Rectangle {
        id: inTrack
        color: info.status === "giaeffettuate" ?
                 (info.delay > 10 ? "#fc3737" : (info.delay > 3 ? "#fcce37" : "#5efc37"))
               : "#cccccc"
        y: parent.y
        x: (parent.width - width) / 2
        width: parent.size / parent.ratio
        height: parent.height / 2
        visible: !info.first
    }

    Rectangle {
        id: outTrack
        color: info.status === "giaeffettuate" ?
                   ((info.t_real === 0 || info.t_real + 2 > now) ? "#cccccc"
                         : (info.delay > 10 ? "#fc3737" : (info.delay > 3 ? "#fcce37" : "#5efc37"))
                    )
               : "#cccccc"
        y: parent.y + parent.height / 2
        x: (parent.width - width) / 2
        width: parent.size / parent.ratio
        height: parent.height / 2
        visible: !info.last
    }

     Rectangle {
         id: stopCircle
         color: info.status === "giaeffettuate" ?
                    (info.delay > 10 ? "#fc3737" : (info.delay > 3 ? "#fcce37" : "#5efc37"))
                : "#cccccc"
         anchors.centerIn: parent
         width: parent.size
         height: parent.size
         radius: width / 2
         border.color: "#ffffff"
         border.width: info.current ? 8 : 4
     }
}
