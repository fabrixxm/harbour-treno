import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {
    id: delegate
    anchors {
        left: parent.left
        right: parent.right
    }
    contentHeight: column.height + (Theme.paddingSmall * 2)

    property string timetable;

    Column {
        id: column
        spacing: Theme.paddingSmall
        anchors {
            leftMargin: Theme.paddingLarge
            rightMargin: Theme.paddingLarge
            topMargin: Theme.paddingSmall
            left: parent.left
            right: parent.right
            top:parent.top
        }
        Label {
            text: (timetable === "departures" ? qsTr("%2 : to %1") : qsTr("%2 : from %1"))
                    .arg(model.to)
                    .arg(model.start)
            width: parent.width
            color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
        }
        Label {
            text: model.name
            width: parent.width
            color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
            font.pixelSize: Theme.fontSizeTiny
            truncationMode: TruncationMode.Fade
        }
    }
}
