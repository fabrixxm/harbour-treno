import QtQuick 2.0
import Sailfish.Silica 1.0

Label {
    anchors {
        leftMargin: Theme.paddingMedium
        rightMargin: Theme.paddingMedium
        left: parent.left
        right: parent.right
    }
    text: section === "train" ? qsTr("Trains") : qsTr("Stations")
    horizontalAlignment: Text.AlignRight
    color: Theme.secondaryHighlightColor
}
