import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {
    id: delegate


    contentHeight: stationLabel.height + timeLabels.height + (Theme.paddingSmall * 2)
    anchors {
        left: parent.left
        right: parent.right
    }

    StopGraph {
        id: stopGraph
        info: model
    }

    Label {
        id: stationLabel
        text: model.station
        color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
        truncationMode: TruncationMode.Fade
        font.bold: model.current
        anchors {
            top: parent.top
            left: stopGraph.right
            right: parent.right
            topMargin: Theme.paddingSmall
            leftMargin: Theme.paddingMedium
            rightMargin: Theme.paddingMedium
        }
    }
    Row {
        id: timeLabels
        anchors {
            top: stationLabel.bottom
            left: stopGraph.right
            right: parent.right
            topMargin: Theme.paddingSmall
            leftMargin: Theme.paddingMedium
            rightMargin: Theme.paddingMedium
        }

        spacing: Theme.paddingSmall

        Label {
            id: programLabel
            text: model.program
            color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
            font.pixelSize: Theme.fontSizeTiny
        }

        Label {
            id: realLabel
            text: (model.status === "giaeffettuate" ?
                       qsTr("actual: %1") : qsTr("expected: %1")
                    ).arg(model.real)
            color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
            font.pixelSize: Theme.fontSizeTiny
            visible: model.real !== "" && model.real !== model.program
            font.bold: true;
        }

        Label {
            id: ritardoLabel
            text: qsTr("delay: %1").arg(model.delay)
            color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
            font.pixelSize: Theme.fontSizeTiny
            font.bold: true;
            visible: model.delay > 0
        }

    }
}
