import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {
    property int size: 32
    id: delegate
    anchors {
        left: parent.left
        right: parent.right
    }
    contentHeight: theLabel.height + (Theme.paddingSmall * 2)

    /*Column {
        id: column
        spacing: Theme.paddingSmall
        anchors {
            leftMargin: Theme.paddingLarge
            rightMargin: Theme.paddingLarge
            topMargin: Theme.paddingSmall
            left: parent.left
            right: parent.right
            top:parent.top
        }*/

    Rectangle {
        id: severityCircle
        anchors {
            leftMargin: Theme.paddingLarge
            left: parent.left
            verticalCenter: parent.verticalCenter
        }

        width: size
        height: size
        color: [Theme.highlightColor, Theme.highlightColor, "#e6e600", "#e60000"][model.severity_code]
        radius: width / 2
    }

    Label {
        id: theLabel
        text: model.descrizione

        anchors {
            leftMargin: Theme.paddingMedium
            rightMargin: Theme.paddingLarge
            left: severityCircle.right
            right: parent.right
            verticalCenter: parent.verticalCenter
        }

        color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
        truncationMode: TruncationMode.Elide
    }
}
