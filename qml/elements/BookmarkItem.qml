import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

ListItem {
    id: delegate
    anchors {
        left: parent.left
        right: parent.right
    }
    contentHeight: column.height + (Theme.paddingSmall * 2)

    property bool isCurrent: globals.train && globals.train.name === model.name

    Column {
        id: column
        spacing: Theme.paddingSmall
        anchors {
            leftMargin: Theme.paddingLarge
            rightMargin: Theme.paddingLarge
            topMargin: Theme.paddingSmall
            left: parent.left
            right: isCurrent ? stopGraph.left :  parent.right
            top:parent.top
        }
        Label {
            text: model.name + (isCurrent ?  ": " + globals.train.current.station : "")
            width: parent.width
            color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
        }
        Label {
            visible: model.type === "train"
            text: "%1 (%3) - %2 (%4) %5"
                    .arg(model.from)
                    .arg(model.to)
                    .arg(model.start)
                    .arg(model.end)
                    .arg(isCurrent && globals.train.current.delay >0  ?  " +" + globals.train.current.delay : "")
            width: parent.width
            color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
            font.pixelSize: Theme.fontSizeTiny
            truncationMode: TruncationMode.Fade
        }
    }

    StopGraph {
        id: stopGraph
        info: isCurrent ? globals.train.current :  {}
        visible: isCurrent
        anchors {
            left: undefined
            right: parent.right
        }
        width: 64

    }



}
