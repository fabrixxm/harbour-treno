import QtQuick 2.0
import QtGraphicalEffects 1.0
import Sailfish.Silica 1.0
import "../elements"
import "../models"

Page {
    id: page

    JSONListModel {
        id: bookmarksModel
        json: conf.bookmarks
        paginationCount: 1000
    }

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaListView {
        id: bookmarkList
        anchors.fill: parent

        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {
            MenuItem {
                text: qsTr("News")
                onClicked: pageStack.push(Qt.resolvedUrl("AvvisiPage.qml"))
            }

            MenuItem {
                text: qsTr("Real time lines")
                onClicked: pageStack.push(Qt.resolvedUrl("LineePage.qml"))
            }

            MenuItem {
                text: qsTr("Search train #")
                onClicked: pageStack.push(Qt.resolvedUrl("SearchTrain.qml"))
            }

            MenuItem {
                text: qsTr("Search station")
                onClicked: pageStack.push(Qt.resolvedUrl("SearchStation.qml"))
            }
        }

        header:PageHeader {
            id: pageHeader
            title: "TreNó"
            Image {
                id: icon
                source: Qt.resolvedUrl("../harbour-treno-symbolic.svg")
                sourceSize: Qt.size(86, 86)
                smooth: true
                anchors {
                    verticalCenter: parent.verticalCenter
                    leftMargin: Theme.paddingLarge
                    left: parent.left
                }
                visible: false;
            }
            ColorOverlay {
                anchors.fill: icon
                source: icon
                color: Theme.secondaryHighlightColor
            }
        }


        model: bookmarksModel.model

        section.property: "type"
        section.criteria: ViewSection.FullString
        section.delegate: BookmarkSectionHeading {}

        delegate: BookmarkItem {

            id: delegate
            onClicked: {
                console.log(params.numeroTreno, params.codLocOrig);
                var page;
                switch(type) {
                    case "train": page = Qt.resolvedUrl("TrenoPage.qml"); break;
                    case "station": page = Qt.resolvedUrl("StazionePage.qml"); break;
                }

                pageStack.push(page, params)
            }
            menu: ContextMenu {
                MenuItem {
                    text: qsTr("Remove from bookmarks")
                    RemorseItem { id: remorse }
                    function showRemorseItem() {
                        var idx = index
                        remorse.execute(delegate, qsTr("Removing bookmark"), function() { conf.removeBookmark(idx) } )
                    }
                    onClicked: showRemorseItem()
                }
            }
        }

        VerticalScrollDecorator { flickable: bookmarkList }
    }

    Label {
        anchors.fill: parent
        text: qsTr("No bookmarks yet.\nPull down to search.")
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        visible: globals.bookmarks.length === 0
    }


}
