import QtQuick 2.0
import Sailfish.Silica 1.0

import "../models"
import "../elements"

import "../utils.js" as Utils

Page {
    id: page


    JSONListModel {
        id: linesStatusModel;
        source: "http://m.trenord.it/direttrici"
        function theUrl() { return source; }
        function sortJson(objectArray) {
            return objectArray.sort(function(a,b){
                return ('' + a.nome).localeCompare(b.nome);
            });
        }
    }

    SilicaListView {
        id: lineeList
        model: linesStatusModel.model
        anchors.fill: parent


        PullDownMenu {
            MenuItem {
                text: qsTr("Refresh")
                onClicked: linesStatusModel.reload()
            }
        }

        header : PageHeader {
            title: qsTr("Real time lines")
            /* description: trenoModel.current  ? trenoModel.current.formatDescription() : "" */
        }

        delegate: LineaItem {
            onClicked: {
                pageStack.push(Qt.resolvedUrl("LineaDetailPage.qml"), {
                                            'descrizione': model.descrizione,
                                            'avvisi': model.news
                                         })
            }
        }

        VerticalScrollDecorator { flickable: avvisoList }
    }


    LoadingSpinner {
        model: linesStatusModel
    }

}
