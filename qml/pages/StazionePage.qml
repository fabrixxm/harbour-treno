import QtQuick 2.0
import Sailfish.Silica 1.0

import "../models"
import "../elements"

import "../utils.js" as Utils

Page {
    id: page
    property string stazione: ""
    property string codiceStazione: ""
    property string timetable: "departures"

    onTimetableChanged: stazioneModel.reload()

    Component.onCompleted: {
        stazioneModel.reload();
    }

    StazioneModel {
        id: stazioneModel
        timetable: page.timetable
        stazione: page.stazione
        codiceStazione: page.codiceStazione
    }

    SilicaListView {
        id: trenoList
        model: stazioneModel.model
        anchors.fill: parent


        PullDownMenu {
            MenuItem {
                text: qsTr("Refresh")
                onClicked: stazioneModel.reload()
            }

            MenuItem {
                text: qsTr("Show departures")
                onClicked: timetable = "departures"
                visible: timetable !== "departures"
            }
            MenuItem {
                text: qsTr("Show arrivals")
                onClicked: timetable = "arrivals"
                visible: timetable !== "arrivals"
            }
            MenuItem {
                text: qsTr("Add to bookmarks")
                onClicked: {
                    var first = trenoModel.model.get(0);
                    var last = trenoModel.model.get(trenoModel.count-1);
                    conf.addBookmark({
                        'type':'station',
                        'name': stazioneModel.name,
                        'params': { 'codiceStazione': codiceStazione, 'stazione': stazione },
                    });
                }
            }
            MenuItem {
                text: qsTr("Remove bookmark")
                onClicked: console.log("todo");
                visible: false
            }


        }

        header : PageHeader {
            title: stazioneModel.name
            description: timetable === "departures" ?  qsTr("departures") : qsTr("arrivals")
        }

        delegate: TrainItem {
            id: delegate
            timetable: page.timetable
            onClicked: {
                console.log(numeroTreno, codLocOrig);
                pageStack.push(Qt.resolvedUrl("TrenoPage.qml"), {
                                'numeroTreno': numeroTreno,
                                'codLocOrig': codLocOrig
                               })
            }
        }

        VerticalScrollDecorator { flickable: trenoList }
    }


    LoadingSpinner {
        model: stazioneModel
    }

}
