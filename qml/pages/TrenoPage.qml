import QtQuick 2.0
import Sailfish.Silica 1.0

import "../models"
import "../elements"

import "../utils.js" as Utils

Page {
    id: page
    property string numeroTreno: ""
    property string codLocOrig: ""

    Component.onCompleted: {
        trenoModel.numeroTreno = page.numeroTreno;
        trenoModel.codLocOrig = page.codLocOrig;
        trenoModel.reload();
    }
    Component.onDestruction: {
        unsetCurrent();
    }

    /*
    function setCurrent() {
        // status ===  1 : ready

        // set current station if current time of arrival is in 10 minutes
        var now = Utils.currentmin()
        console.log(trenoModel.current, trenoModel.current.t_real + 10, now, trenoModel.current.t_real + 10 < now);
        if (trenoModel.current) {
            if (trenoModel.current.t_real + 10 < now) {
                globals.train = false;
                return
            }
            globals.train = trenoModel
        }
    }
    */


    SilicaListView {
        id: trenoList
        model: trenoModel.model
        anchors.fill: parent


        PullDownMenu {
            MenuItem {
                text: qsTr("Refresh")
                onClicked: trenoModel.reload()
            }
            MenuItem {
                text: qsTr("Add to bookmarks")
                onClicked: {
                    var first = trenoModel.model.get(0);
                    var last = trenoModel.model.get(trenoModel.count-1);
                    conf.addBookmark({
                        'type':'train',
                        'name': trenoModel.name,
                        'from': first.station,
                        'to':   last.station,
                        'start':first.program,
                        'end':  last.program,
                        'params': { 'numeroTreno': numeroTreno, 'codLocOrig': codLocOrig },
                    });
                }
            }
            MenuItem {
                text: qsTr("Remove bookmark")
                onClicked: console.log("todo");
                visible: false
            }

        }

        header : PageHeader {
            title: trenoModel.name
            description: trenoModel.current  ? trenoModel.current.formatDescription() : ""
        }

        delegate: StopItem { }

        VerticalScrollDecorator { flickable: trenoList }
    }


    LoadingSpinner {
        model: trenoModel
    }

}
