import QtQuick 2.0
import Sailfish.Silica 1.0

import "../models"
import "../elements"

import "../utils.js" as Utils

Page {
    id: page
    property string descrizione: ""
    property variant avvisi: []

    SilicaListView {
        id: theList
        model: avvisi
        anchors.fill: parent

        header : PageHeader {
            title: descrizione
            /* description: trenoModel.current  ? trenoModel.current.formatDescription() : "" */
        }

        delegate: LineaNewsItem {}

        VerticalScrollDecorator { flickable: avvisoList }
    }



}
