import QtQuick 2.0
import Sailfish.Silica 1.0

import "../models"
import "../elements"

import "../utils.js" as Utils

Page {
    id: page

    AvvisiModel {
        id: avvisiModel;
        onStatusChanged: {
            console.log(status);
        }

    }

    SilicaListView {
        id: avvisoList
        model: avvisiModel
        anchors.fill: parent


        PullDownMenu {
            MenuItem {
                text: qsTr("Refresh")
                onClicked: avvisiModel.reload()
            }
        }

        header : PageHeader {
            title: qsTr("News")
            /* description: trenoModel.current  ? trenoModel.current.formatDescription() : "" */
        }

        delegate: AvvisoItem {
            onClicked: {
                pageStack.push(Qt.resolvedUrl("AvvisoDetailPage.qml"), {
                                          'avviso': model
                                         })
            }
        }

        VerticalScrollDecorator { flickable: avvisoList }
    }


    LoadingSpinner {
        status: trenoModel.status
    }

}
