import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page
    property QtObject avviso;

    SilicaFlickable {
        id: flickable
        anchors.fill: parent;
        contentHeight: column.height + (Theme.paddingLarge * 2)

        PullDownMenu {
            MenuItem {
                text: qsTr("Open web page")
                onClicked: Qt.openUrlExternally(avviso.link);
            }
        }


        Column {
            id: column
            anchors {
                leftMargin: Theme.paddingLarge
                rightMargin: Theme.paddingLarge
                topMargin: Theme.paddingLarge
                left: parent.left
                right: parent.right
                top:parent.top
            }
            spacing: Theme.paddingMedium

            Label {
                width: parent.width
                text: avviso.title
                wrapMode: Text.Wrap
                color: Theme.highlightColor
                horizontalAlignment: Text.AlignRight
            }
            Label {
                text: avviso.pubDate
                width: parent.width
                font.pixelSize: Theme.fontSizeTiny
                truncationMode: TruncationMode.Fade
            }

            Label {
                width: parent.width
                text: avviso.content.replace('src="/', 'src="http://www.trenord.it/').replace('href="/', 'href="http://www.trenord.it/')
                wrapMode: Text.Wrap
            }

            Button {
                width: parent.width
                visible: avviso.content.trim() === ""
                text: qsTr("Open web page")
                onClicked:  {
                    Qt.openUrlExternally(avviso.link);
                }
            }

        }


        VerticalScrollDecorator { flickable: flickable }
    }
}
