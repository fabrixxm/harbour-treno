import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    ListModel {
        id: selectModel
    }

    Column {
        anchors.fill: parent
        spacing: Theme.paddingLarge

        PageHeader { title: qsTr("Search train by number") }
        TextField {
            focus: true
            width: parent.width
            id: numeroTreno
            label: qsTr("Train number")
            placeholderText: qsTr("Train number")
            inputMethodHints: Qt.ImhFormattedNumbersOnly

            EnterKey.enabled: text.length > 0
            //EnterKey.iconSource: "image://theme/icon-m-enter-next"
            EnterKey.onClicked: search(text)
        }


        // se c'è più di un treno disponibile, vengono mostrati qui
        Item {
            height: selectView.contentHeight
            width: parent.width

            /*Rectangle { color: "#db1616"; anchors.fill:parent; }*/

            ListView {
                anchors {
                    leftMargin: Theme.paddingLarge
                    rightMargin: Theme.paddingLarge
                    fill: parent
                }
                id: selectView
                model: selectModel

                delegate: ListItem {
                    contentHeight: labelLabel.height + (Theme.paddingSmall * 2)
                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    id: delegate
                    Label {
                        id: labelLabel
                        text: label
                        color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
                    }
                    onClicked: pageStack.push(Qt.resolvedUrl("TrenoPage.qml"), {
                                        'numeroTreno': numeroTreno,
                                        'codLocOrig': codLocOrig
                                       })

                }
            }
        }


    }

    function search(n) {
        selectModel.clear()

        var theurl = globals.baseurl + "/numero"
        var xhr = new XMLHttpRequest;
        xhr.open("POST", theurl, true);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        xhr.onreadystatechange = function() {
            status = xhr.status
            if (xhr.readyState === XMLHttpRequest.DONE) {

                var html = xhr.responseText.replace(/[\r\n]/g, "");


                if (html.match(/<select/)) { // ooh! a select!
                    html.match(/<option value="([^"]+)">([^<]+)<\/option>/g).forEach(function(s){
                        var opt = s.match(/<option value="([^"]+)">([^<]+)<\/option>/);
                        var params = opt[1].split(";");
                        console.log(opt[2], params[0], params[1]);
                        selectModel.append({
                                               'label': opt[2],
                                               'numeroTreno': params[0],
                                               'codLocOrig': params[1]
                                           });
                    })
                    return;
                }

                var paramstr = html.match(/href="[^?]*\?([^"]*)"/)[1];

                if (paramstr === undefined) return;
                paramstr.replace("&amp;", "&");

                var params = {};
                paramstr.split("&").forEach(function(tok){
                    var p = tok.split("=");
                    params[p[0]] = p[1];
                });

                console.log(params["numeroTreno"], params["codLocOrig"]);
                pageStack.push(Qt.resolvedUrl("TrenoPage.qml"), {
                                'numeroTreno': params["numeroTreno"],
                                'codLocOrig': params["codLocOrig"]
                               })
            }
        }
        xhr.send("numeroTreno="+n+"&tipoRicerca=numero&lang=IT");
    }

}
