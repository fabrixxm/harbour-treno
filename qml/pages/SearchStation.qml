import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    ListModel {
        id: selectModel
    }

    SilicaListView {
        id: selectView
        model: selectModel
        anchors.fill: parent;

        header: Column {
            width: parent.width
            spacing: Theme.paddingLarge

            PageHeader { title: qsTr("Search station") }
            SearchField {
                id: searchField
                width: parent.width
                focus: true
                label: qsTr("Station name")
                placeholderText: qsTr("Station name")
                EnterKey.enabled: text.length > 0
                EnterKey.onClicked: search(text)
            }
        }

        delegate: ListItem {
            property int spacing: Theme.paddingMedium
            contentHeight: labelLabel.height + (spacing * 2)
            anchors {
                left: parent.left
                right: parent.right
            }

            id: delegate
            Label {
                anchors {
                    verticalCenter: parent.verticalCenter
                    right: parent.right
                    left: parent.left
                    leftMargin: spacing
                    rightMargin: spacing
                }

                id: labelLabel
                text: label
                color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
            }
            onClicked: pageStack.push(Qt.resolvedUrl("StazionePage.qml"), {
                          'stazione': "",
                          'codiceStazione': codiceStazione
                         })

        }


        VerticalScrollDecorator {}
    }

    function search(name) {
        selectModel.clear()

        var theurl = globals.baseurl + "/stazione"
        var xhr = new XMLHttpRequest;
        xhr.open("POST", theurl, true);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        xhr.onreadystatechange = function() {
            status = xhr.status
            if (xhr.readyState === XMLHttpRequest.DONE) {

                var html = xhr.responseText.replace(/[\r\n]/g, "");


                if (html.match(/<select/)) { // ooh! a select!
                    html.match(/<option value="([^"]+)">([^<]+)<\/option>/g).forEach(function(s){
                        var opt = s.match(/<option value="([^"]+)">([^<]+)<\/option>/);
                        var params = opt[1].split(";");
                        //console.log(opt[2], params[0]);
                        selectModel.append({
                           'label': opt[2],
                           'codiceStazione': params[0]
                       });
                    })
                    return;
                }

                pageStack.push(Qt.resolvedUrl("StazionePage.qml"), {
                                'stazione': name,
                                'codiceStazione': ""
                               })
            }
        }
        xhr.send("stazione="+name+"&lang=IT");
    }

}
