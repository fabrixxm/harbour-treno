import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0

import "pages"
import "models"
import "utils.js" as Utils


ApplicationWindow
{
    id: window
    property int kStandardAnimationDuration: 200

    Item {
        id: globals
        property string baseurl: "http://mobile.my-link.it/mylink/mobile"

        property variant bookmarks: []

        // treno corrente, con una stazione corrente, da vedere nella cover
        property variant train: false;
    }



    ConfigurationGroup {
        id: conf
        path: "/apps/harbour-treno"


        property string bookmarks: '[]'


        function resetBookmarks() {
            bookmarks = '[{"type":"train","name":"REG 165","from":"M N CADORNA","to":"COMO NORD LAGO","start":"17:43","end":"18:44","params":{"numeroTreno": "165","codLocOrig":"N00001"}}]';
        }

        function addBookmark(obj) {
            if (obj.type === "train") {
                globals.bookmarks.unshift(obj);
            } else {
                globals.bookmarks.push(obj);
            }
            bookmarks = JSON.stringify(globals.bookmarks);
        }

        function removeBookmark(idx) {
            globals.bookmarks.splice(idx, 1);
            bookmarks = JSON.stringify(globals.bookmarks);
        }


        onValueChanged: {
            console.log(key);
            globals.bookmarks = JSON.parse(bookmarks); // still not nice..
        }
        Component.onCompleted: {
            console.log(bookmarks);
            globals.bookmarks = JSON.parse(bookmarks);
        }

    }


    initialPage: Component { Bookmarks {} }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations

    Component.onCompleted: checkBookmarks();

    function unsetCurrent() {
        globals.train = false;
        trenoModel.numeroTreno = "";
        trenoModel.codLocOrig = "";
        checkBookmarks();
    }

    function setCurrent() {
        /*// set current station if current time of arrival is in 10 minutes
        var now = Utils.currentmin()
        console.log("setCurrent() -> " , trenoModel.current);
        if (trenoModel.current) {
            console.log(trenoModel.current.t_real + 10, "<", now, "=", trenoModel.current.t_real + 10 < now);
            if (trenoModel.current.t_real + 10 < now) {
                globals.train = false;
                return
            }
            globals.train = trenoModel
        }*/

        if (trenoModel.name === undefined || trenoModel.name === "") return;
        console.log("Set current", trenoModel.name, "status", trenoModel.status);
        globals.train = trenoModel
    }

    // set current train
    // if one of the bookmarks is "current" (now is between start and end)
    // (re)load train info
    function checkBookmarks() {
        var now = Utils.currentmin()
        globals.bookmarks.filter(function(o) { return o.type === "train"}).forEach(function(o){
            var t_start = Utils.hm2min(o.start);
            var t_end = Utils.hm2min(o.end);
            console.log(o.type,  o.name,
                        o.start , "("+t_start+")",
                        o.end, "("+t_end+")",
                        ":", now);
            if (now > t_start && now < t_end + 5 ) {
                console.log("current bookark", o.name);
                if (globals.train !== false && globals.train.name !== o.name) {
                    console.log("there is already a train on cover");
                    return;
                }
                console.log("set", o.name, "as current train");
                trenoModel.numeroTreno = o.params.numeroTreno;
                trenoModel.codLocOrig = o.params.codLocOrig;

            }

            if (now > t_end + 5 && globals.train !== false && globals.train.name === o.name) {
                console.log("remove current train from cover")
                globals.train = false;
            }
        });
        trenoModel.reload();
    }



    Timer {
        id: coverTrenoTimer
        interval: 30000 // 30 sec
        repeat: true
        running: true
        triggeredOnStart: false;
        onTriggered:checkBookmarks()
    }


    TrenoModel {
        id: trenoModel
        /*numeroTreno: page.numeroTreno
        codLocOrig: page.codLocOrig*/

        onStatusChanged: {
            console.log("trenoModel onStatusChanged", status);
            if (status > 4) setCurrent()
        }
    }

}
