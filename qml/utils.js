.pragma library


/**
 * "hh:mm" (str) -> min (int)
 */
function hm2min(hm) {
    if (hm === undefined) return 0;
    if (hm.indexOf(":")<0) {
        return parseInt(hm);
    }

    var t = hm.split(":");
    return parseInt(t[0]) * 60 + parseInt(t[1]);
}

/**
 * current hh:mm as mins
 */
function currentmin() {
    var d = new Date();
    return d.getHours() * 60 + d.getMinutes()
}
